#!/usr/bin/python3

########################
from netmiko import ConnectHandler
from netmiko import NetMikoAuthenticationException
from netmiko import NetMikoTimeoutException
from netmiko import NetmikoTimeoutError
import time
import os
import sys
path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(path)
os.chdir(path)
import socket
import string
import csv
import ipaddress
import argparse
import re
import time
import subprocess
import tempfile
from getpass import getpass
import logging
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import datetime


def router_connect(start_ts, router, username, password, *command):
    try:
        platform = "cisco_ios"
        device = ConnectHandler(device_type=platform, ip=router, username=username, password=password)

        if len(command) <1:
            #temp_file = []
            command = command[0]
            output = device.send_command(command)
            print(command)
            print(output)
            #temp_file.append(command)
            #temp_file.append(output)
            device.disconnect()
            #html_file(router, temp_file)
            time.sleep(10)
            second_option = input("Would you like to check another device? [y/n]: ").strip()
            if "y" in second_option.lower():
                main()
            else:
                print("Goodbye")
                sys.exit()
        elif len(command) >= 2:
            for count, thing in enumerate(command):
                output=device.send_command("{0}".format(thing))
                print(thing)
                print(output)
                #temp_file.append(thing)
                #temp_file.append(output)
            device.disconnect()
            time.sleep(10)
            #html_file(router, temp_file)
            second_option = input("Would you like to connect to another device: [y/n]: ").strip()
            if "y" in second_option.lower():
                    main()
            else:
                print("Goodbye")
                sys.exti()
    except (EOFError, NetMikoTimeoutException, NetMikoAuthenticationException):
        print("Device is unreachable" + "\n" + "Please Call Jeremy")
        time.sleep(1.5)
        print(router + "appears to be offline.  Please Investigate")
    except KeyboardInterrupt:
        print("Goodbye")
        time.sleep(10)

def html_file():
    send_file = (router + ".txt")
    with open(send_file, "w") as f:
        f.write("           " + router + "\n\n")
        for i in temp_file:
            f.write(i + "\n")
    with open(send_file, "r") as f:
        r = f.read()
        print(r)
        time.sleep(10)

def bgp_ts(start_ts, router, username, password):
    command = "show ip bgp sum"
    command1 = "show logging | i BGP"
    router_connect(start_ts, router, username, password, command, command1)

def acl_ts(start_ts, router, username, password):
    get_acl = input("What would you like to veiw?:" + "\n" +
                    "1) acl101" +"\n" +
                    "2) acl166" + "\n" +
                    "3) both?" + "\n" +
                    "Enter Selection:   ")
    print("\n\n")
    if "101" in get_acl or int(get_acl) == 1:
        while True:
            acl101 = input("1) Full" + "\n" +
                           "2) Mathces Only" + "\n" +
                           "3) Search IP address" + "\n" +
                           "Enter Selection:    ")
            if "full" in acl101.lower() or int(acl101) == 1:
                command = "show ip access-lists acl101"
            elif "mat" in acl101.lower() or int(acl101) ==2:
                command = "show ip access-lists acl101 | i matches"
            elif "ip" in acl101.lower() or int(acl101) ==3:
                while True:
                    ip_search = input("Enter IP Address:  ")
                    try:
                        if ipaddress.IPv4Address(ip_search).version == 4:
                            command = "show ip access-lists acl101 | i " + ip_search
                            break
                    except KeyboardInterrupt:
                        print("Goodbye")
                        time.sleep(10)
                        sys.exti()
            else:
                print("That is not a valid option")
                time.sleep(1.5)
                print("Goodbye")
                sys.exit()
            router_connect(start_ts, router, username, password, command)

    elif "166" in get_acl or int(get_acl) == 2:
        while True:
            acl101 = input("1) Full" + "\n" +
                           "2) Mathces Only" + "\n" +
                           "3) Search IP address" + "\n" +
                           "Enter Selection:    ")
            if "full" in acl101.lower() or int(acl101) == 1:
                command = "show ip access-lists acl166"
            elif "mat" in acl101.lower() or int(acl101) ==2:
                command = "show ip access-lists acl166 | i matches"
            elif "ip" in acl101.lower() or int(acl101) ==3:
                while True:
                    ip_search = input("Enter IP Address:  ")
                    try:
                        if ipaddress.IPv4Address(ip_search).version == 4:
                            command ="show ip access-lists acl166 | i " + ip_search
                            break
                    except ipaddress.AddressValueError:
                        print("That is not a valid IP Address")
                        print("\n\n")
                        time.sleep(1.5)
                        print("Pleas enter a valid IP address")
                        print("\n\n")
                        time.sleep(1.5)
                    except KeyboardInterrupt:
                        print("Goodbye")
                        sys.exit()
            else:
                print("That is not a valid option")
                time.sleep(1.5)
                print("Goodbye")
                sys.exit()
            router_connect(start_ts, router, username, password, command)

    elif "both" in get_acl.lower() or int(get_acl) == 3:
        while True:
            acl101 = input("1) Full" + "\n" +
                           "2) Mathces Only" + "\n" +
                           "3) Search IP address" + "\n" +
                           "Enter Selection:    ")
            if "full" in acl101.lower() or int(acl101) == 1:
                command = "show ip access-lists acl166"
                cmmand1 - "show ip access-lists acl101"
                router_connect(start_ts, router, username, password, command, command1)
            elif "mat" in acl101.lower() or int(acl101) ==2:
                command = "show ip access-lists acl166 | i matches"
                command1 = "show ip access-lists acl101 | i matches"
                router_connect(start_ts, router, username, password, command, command1)
            elif "ip" in acl101.lower() or int(acl101) ==3:
                while True:
                    ip_search = input("Enter IP Address:  ")
                    try:
                        if ipaddress.IPv4Address(ip_search).version == 4:
                            command ="show ip access-lists acl166 | i " + ip_search
                            command1 ="show ip access-lists acl101 | i " + ip_search
                            router_connect(start_ts, router, username, password, command, command1)
                            break
                    except ipaddress.AddressValueError:
                        print("That is not a valid IP Address")
                        print("\n\n")
                        time.sleep(1.5)
                        print("Pleas enter a valid IP address")
                        print("\n\n")
                        time.sleep(1.5)
                    except KeyboardInterrupt:
                        print("Goodbye")
                        sys.exit()
            else:
                print("That is not a valid option")
                time.sleep(1.5)
                print("Goodbye")
                sys.exit()
    else:
        print("Not a valid option")

def eigrp_ts(start_ts, router, username, password):
    command = "show ip eigrp neighbors"
    router_connect(start_ts, router, username, password, command)

def int_ts(start_ts, router, username, password):
    command = "show ip int brief"
    command1 = "show int gi0/1"
    router_connect(start_ts, router, username, password, command, command1)

def rate_ts(start_ts, router, username, password):
    command = "show run | i service-policy"
    router_connect(start_ts, router, username, password, command)

def ipa_ts(start_ts, router, usernme, password):
    command = "show ip accounting"
    command = "show ip cache flow"
    router_connect(start_ts, router, username, password, command, command1)

def route_check(start_ts, router, username, password):
    while True:
        ip_search = input("Enter IP Address:    ")
        try:
            if ipaddress.IPv4Address(ip_search).version == 4:
                command = "show ip route " + ip_search
                break
        except KeyboardInterrupt:
            print("\n\n")
            print("Goodbye")
            sys.exit()
        except ipaddress.AddressValueError:
            print("That is not a valid IP address")
            print("\n\n")
            time.sleep(1.5)
            print("Please enter a valid IP Address")
            print("\n\n")
            time.sleep(1.5)
def check_logs(start_ts, router, username, password):
    while True:
        ip_search = input("Enter IP Address:    ")
        try:
            if ipaddress.IPv4Address(ip_search).version == 4:
                command = "show logging | i " + ip_search
                router_connect(start_ts, rouer, username, password, command)
                break
        except KeyboardInterrupt:
            print("\n\n")
            print("Goodbye")
            sys.exit()
        except ipaddress.AddressValueError:
            print("That is not a valid IP Address")
            print("\n\n")
            time.sleep(1.5)
            print("Please enter a valid IP address")
            time.sleep(1.5)

def get_router():
    while True:
        try:
            subprocess.call("clear", shell=True)
            router = input("Please enter the SMART router youwould like to view:    ").strip()
            print("\n\n")
            if "ma" in router.lower() or "mv" in router.lower():
                if "ma" in router.lower():
                    att = "cma"
                    num = router[-4:]
                    router = att + num
                elif "mv" in router.lower():
                    att = "cmv"
                    num = router[-4:]
                    router = att + num
                return(router)
                break
            else:
                print("That is not a valid Router.")
                print("\n\n")
                time.sleep(1.5)
                print("Please enter a valid Router ID")
                print("\n\n")
                time.sleep(1.5)
        except KeyboardInterrupt:
            print("\n\n")
            print("Goodbye")
            sys.exit()

def main():
    try:
        username = "jfriese"
        password = ""
        router = get_router()
        print("\n\n")
        start_ts = input("What are you troubleshooting, Enter number option:" + "\n" +
                         "1) IP Accounting" + "\n" +
                         "2) Rate Limit"  + "\n" +
                         "3) EIGRP"  + "\n" +
                         "4) Interface"  + "\n" +
                         "5) ACL"  + "\n" +
                         "6) Check if route is present for IP address"  + "\n" +
                         "7) Check logs for IP address"  + "\n" +
                         "Enter your selection:    ").strip()
        print("\n\n")
        if "rate" in start_ts.lower() or int(start_ts) == 2:
            rate_ts(start_ts, router, username, password)
        elif "bgp" in start_ts.lower() or int(start_ts) == 8:
            bgp_ts(start_ts, router, username, password)
        elif "eigrp" in start_ts.lower() or int(start_ts) == 3:
            eigrp_ts(start_ts, router, username, password)
        elif "int" in start_ts.lower() or int(start_ts) == 4:
            int_ts(start_ts, router, username, password)
        elif "acl" in start_ts.lower() or int(start_ts) == 5:
            acl_ts(start_ts, router, username, password)
        elif "route" in start_ts.lower() or int(start_ts) == 6:
            route_ts(start_ts, router, username, password)
        elif "log" in start_ts.lower() or int(start_ts) == 7:
            check_logs(start_ts, router, username, password)
        else:
            print("That is not a valid option")
            sys.exit()
    except KeyboardInterrupt:
        print("\n\n")
        print("Goodbye")
        sys.exit()

if __name__ == "__main__":
    main()

