#!/usr/bin/python3
from netmiko import ConnectHandler
from netmiko import NetMikoAuthenticationException, NetMikoTimeoutException
from netmiko import NetmikoTimeoutError
import logging
import subprocess
import ipaddress
import sys
import os
import fcntl
import csv
import urllib
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
import time
import re
import datetime, threading
import socket
from email import encoders
from getpass import getpass

def ping_from_device():
    try:
        username = input("Enter Username: ")
        password = getpass("Enter Password: ")
        platform = "cisco_nxos"
        host = input("Enter device to log in to:")
        remote_IP = input("Enter remote device to test connectivity: ")
        logging.basicConfig(filename="Device.log", level = logging.DEBUG)
        logger = logging.getLogger("netmiko")
        device = ConnectHandler(device_type = platform, ip=host, username=username, password=password)
        while True:
            time.sleep(60)
            command = f"ping {remote_IP} packet-size 1500 count 30"
            output = device.send_command(command)
            with open("Device.txt", "w") as b:
                b.write(output)
            f = open("Device.txt")
            new_file = f.read()
            f.close()
            a = new_file.replace("   ", ",")
            b = a.replace("   ", ",")
            c = b.replace("  ", ",")
            d = c.replace(" ", ",")
            e = d.replace(",,,", ",")
            k = e.replace(",,", ",")
            new_str = k.replace(" ", ",")
            with open("device_a.txt", "w+") as f:
                f.write(new_file)
            csvfile = open("Device.csv", "w")
            writer = csv.writer(csvfile, delimiter = ",")
            with open("Device_a.txt", "r") as f:
                rows = csv.reader(f)
                for line in rows:
                    if "1508" in line:
                        writer.writerow(line)
                        test = line[6]
                        test = re.sub("time=", "", test)
                        print(test)
                    if float(test) >= 50 or "timed out" in output:
                        print("Pings Failed.  Contact NSI")
                        send_email()
            csvfile.close()
    except KeyboardInterrupt:
        device.disconnect()
        sys.exit()
    except (EOF, NetMikoTimeoutException, NetMikoAuthenticationException):
        print("ERROR")
        sys.exit()
def send_email():
    msg = MIMEMultipart()
    fromaddress = "thefriese3@gmail.com"
    toaddress = "darthfriese1@gmail.com"
    a = open("test.txt", "r")
    msg.attach(MIMEText(a.read())
    a.close()
    attachment = MIMEBase("text", "octet-stream")
    server = smtplib.SMTP("Server Goes Here")
    server.sendmail(fromaddress, toaddress, msg.as_string())
    server.quit()

if __name__ == "__main__":
    ping_from_device()
