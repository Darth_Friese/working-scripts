import pandas as pd
import xlswritter
from datatime import datetime

def writers(onelist, startlist, endlist, times, output):
    df = pd.DataFrame.from_dict({"ID": onelist, "StartTIme": startlist, "EndTime": endlist, "Timediff": times})
    writer - pd.ExcelWriter(output, engine=)"xlswriter")
    df.to_excel(writer, sheet_name="TimeCalc", index=False)
    df.style.applymap(backgroundColor, subset=[''])
    workbook = writer.book
    worksheet = writer.sheets[""]
    worksheet.set_column('A:A', 18)
    worksheet.set_column('B:B', 18)
    worksheet.set_column('C:C', 18)
    """
    worksheet.set_column('D:D', 116)
    worksheet.set_column('E:E', 25)
    worksheet.set_column('F:F', 25)
    worksheet.set_column('G:G', 25)
    worksheet.set_column('H:H', 25)
    worksheet.set_column('I:I', 30)
    """
    border_fmt = workbook.add_format({'bottom': 1, 'top': 1, 'left': 1, 'right': 1})
    format1 = workbook.add_format({'bg_color': '#FFC7CE',
                                'font_color': '#9C006'})
    format2 = workbook.add_format({'bg_color': '#C6EFCE',
                                   'font_color': '#006100'})
    worksheet.conditional_format('D2:D1000', {'type': 'cell',
                                              'criteria': '>=',
                                              'value': 50,
                                              'format': format1})
    worksheet.conditional_format('D2:D1000', {'type': 'cell',
                                              'criteria': '<',
                                              'value': 50,
                                              'format': format2})
    worksheet.conditional_format(xlswritter.utility.xl_range(0, 0, len(df), len(df.columns)),
    {'type': 'no_errors', 'format': border_fmt})
    writer.save()

def time_diff(rows):
    difflist = []
    for i in rows:
        start = i[0]
        end = i[1]
        diff = end - start
        difflist.append(diff.total_seconds())
    return(difflist)

def main():
    files = 'name of ingested file in directory'
    output = 'name of file outputed'
    startlist, endlist, onelist = [], [], []
    df = pd.read_excel (files, sheet_name='Tasks')
    for i in df["startTIme"]:
        startlist.append(i)
    for k in df["endTIme"]:
        endlist.append(k)
    for a in df["id"]:
        onelist.append(a)
    rows = zip(startlist, endlist)
    times = timeDiff(rows)
    rows2 = zip(startlist,endlist,times)
    writers(onelist, startlist, endlist, times, output)

if __name__ == "__main__":
    main()
